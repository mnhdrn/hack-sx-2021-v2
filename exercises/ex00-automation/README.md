# EX00 - Automatisation

- Remplisser le playbook main.yml dans ansible-project de manières
	à creer l'arborescence suivante :

	Linux-history
	|_ RedHat
	|  |_ ascii-RedHat.txt
	|_ Fedora
	|  |_ ascii-Fedora.txt
	|_ CentOs
	|  |_ ascii-Centos.txt
	|_ Oracle
	|  |_ ascii-Oracle.txt
	|_ Debian
	|  |_ ascii-Debian.txt
	|_ Parrot
	|  |_ ascii-Parrot.txt
	|_ Ubuntu
	|  |_ ascii-Ubuntu.txt
	|_ Kali Linux
	|  |_ ascii-Kali.txt

- Les repertoires devront avoir les droits suivant 740
- corriger et Lancer le playbook
